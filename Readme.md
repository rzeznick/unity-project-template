# Unity Setup Tools

## Descritpion

Since v0.3 this project changed its purpose from project structure guidelines (which can be found [here](Documentation/UnityProjectStructureGuideline.md)) and project template into the setup tools that can be added to the fresh Unity project.

## Setup Functions

* Create Default Folder Structure - following [this guidelines](Documentation/UnityProjectStructureGuideline.md). It also creates Systems Prefab in resources folders.
* Replace manifest file with the one that keep minimum installed packages

## Quick Navigation Functions

* Edit Systems - SHIFT+S
* Highlight scenes folder - SHIFT-A
* Highlight prefab folder - SHIFT-Z
* Open All Scenes - SHIFT-W


## KNOWN ISSUES

* Systems Prefab is added to currently opened scene, while creating default folders. It is **VERY IMPORTANT** to remove this prefab from the scene.
