using UnityEngine;

namespace Metzger.Core
{
    /// <summary>
    /// Project Initialization taken from this tut - https://youtu.be/zJOxWmVveXU 
    /// </summary>
    public class Bootstrapper
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Execute() => LoadSystems();

        private static void LoadSystems()
        {
            Object.DontDestroyOnLoad(Object.Instantiate(Resources.Load("Systems")));
        }
    }
}