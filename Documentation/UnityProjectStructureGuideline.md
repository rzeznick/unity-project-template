# Unity Project Structure Guideline

## Rule of thumb

1. **Do not store any asset files in the root directory.** Use subdirectories whenever possible.
2. **Do not create any additional directories in the root directory,** unless you really need to.
3. **Be consistent with naming.** If you decide to use camel case for directory names and low letters for assets, stick to that convention.
4. ** Don�t try to move context-specific assets to the general directories.** For instance, if there are materials
generated from the model, don�t move them to Materials directory because later, you won�t know where these come from.
5. Use 3rd-Party to store assets imported from the Asset Store. They usually have their own structure that shouldn�t be altered.
6. Use Temp/Sandbox/Purgatory directory for any experiments you�re not entirely sure about. While working on this kind of
thing, the last thing you want to care about is a proper organization. Do what you want, then remove it or organize it when you�re sure that you want to include it in your project. ~~When you�re working
on a project with other people, create your personal Temp/Sandbox subdirectory like Sandbox/JohnyC.~~ From my experience, the Temp folder is not something that you want to share or put in the version control system. Therefore there is no need to localize it for users. Also, I strongly suggest using **Sandbox** or **Purgatory** for folder name as **Temp** name is tracked by project level .gitignore and will prevent it from keeping it empty in GIT.

## Basic rules

* Data is for runtime data that the game will need that isn�t necessarily art, code or audio - such as
    * scriptable objects
    * asset data
    * materials
    * profiles
    * volumes
    * scenes
    * ... the other runtime stuff.

* **Prefabs** go in data of course because they�re often instantiated. Some folders inside **Data** are actually special folders, that is, the editor in the background keeps an asset populated with the links to these
prefabs, so that you can reach any asset you want.

* Basically just group by file type or purpose inside Data. It�s runtime accessible and easy for you to port to addressable or other schemes later. Data means game data.

* Keeping a project clean is a **daily task**, like the house or whatever, it�s just something you should *never* let become disorganized because you will harm project development pretty much right away.

* The rules in general for organizing is: if it can be made flatter, keep it flatter, don�t nest unless forced to.
You will be able to recognize when you need to go deeper when it becomes a bit too hard to find a specific type inside.

* Audio source files are in the Audio folder, but at runtime, if you have prefabs, they will be prefabs, with audio in data. The source files for that stay in audio though. Audio and art are good candidates for separation from runtime data because they�re a surface area other teams or staff or people can touch.

* Having strict and rationale rules for organizing means you can build editor tooling that works with that, that can depend on certain art folders or filenames being preprocessed in a certain way or having an auto-populated list of data you can access at the editor or runtime. Things that keep work less for you by way of consistency.

## Pending approvals

* Gizmos folder in the root folder
* Resources folder in the root folder
* Editor folder in the root folder