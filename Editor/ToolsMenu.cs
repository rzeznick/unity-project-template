using Metzger.Tools.Setup;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using static System.IO.Path;
using static UnityEditor.AssetDatabase;

namespace Metzger.Tools
{
    public static class ToolsMenu
    {
        private const string RelativeRootFolder = "Assets";
        private const string DataFolderName = "Data";
        private const string PrefabsFolderName = "Prefabs";
        private const string ScenesFolderName = "Scenes";
        private const string ResourcesFolderName = "Resources";
        private const string SystemsPrefabName = "Systems";

        [MenuItem("Tools/Metzger/Setup/Create Default Folders")]
        private static void CreateDefaultFolders()
        {
            // create root folders
            Folders.CreateDirectories(string.Empty, "Art", "Audio", "Code", DataFolderName, "Plugins", "Purgatory", ResourcesFolderName, "StreamingAssets", "UnitTests");

            // create Art subfolders
            Folders.CreateDirectories("Art", "Animation", "Fonts", "Materials", "Models", "Terrains", "Textures");

            // create Audio subfolders
            Folders.CreateDirectories("Audio", "Music", "Sfx");

            // create Code subfolders
            Folders.CreateDirectories("Code", "Editor");

            // create Data subfolders
            Folders.CreateDirectories(DataFolderName, PrefabsFolderName, "Profiles", ScenesFolderName, "Settings", "Volumes");

            // create empty resource
            var systemsPrefabPath = Combine(RelativeRootFolder, ResourcesFolderName, SystemsPrefabName + ".prefab");
            if (!LoadAssetAtPath(systemsPrefabPath, typeof(GameObject)))
            {
                PrefabUtility.SaveAsPrefabAssetAndConnect(new GameObject(SystemsPrefabName), systemsPrefabPath, InteractionMode.UserAction);
            }

            // refresh
            Refresh();
        }

        [MenuItem("Tools/Metzger/Setup/Load Minimal Manifest")]
        private static void LoadBareManifest() => Packages.ReplacePackages("b6743e6ad96711d78ec3261d8a91c6cd8c03c447");

        [MenuItem("Tools/Metzger/Quick Nav/Edit Systems #s")]
        public static void OpenSystemsPrefab()
        {
            // Select object in the Inspector
            Selection.activeObject = Resources.Load(SystemsPrefabName);

            // Highlight and blink selected object in Project window
            EditorGUIUtility.PingObject(Selection.activeObject);

            // Open prefab
            OpenAsset(Selection.activeObject);
        }

        [MenuItem("Tools/Metzger/Quick Nav/Highlight scenes folder #a")]
        public static void HighlightScenesFolder()
        {
            // Load object
            var obj = LoadAssetAtPath(Combine(RelativeRootFolder, DataFolderName, ScenesFolderName), typeof(Object));

            // Highlight and blink selected object in Project window
            EditorGUIUtility.PingObject(obj);
        }

        [MenuItem("Tools/Metzger/Quick Nav/Highlight prefab folder #z")]
        public static void HighlightPrefabsFolder()
        {
            // Load object
            var obj = LoadAssetAtPath(Combine(RelativeRootFolder, DataFolderName, PrefabsFolderName), typeof(Object));

            // Highlight and blink selected object in Project window
            EditorGUIUtility.PingObject(obj);
        }

        [MenuItem("Tools/Metzger/Quick Nav/Open All Scenes #w")]
        public static void OpenAllScenes()
		{
			var folderPath = $"{RelativeRootFolder}/{DataFolderName}/{ScenesFolderName}";
			var scenes = FindAssets("t:Scene")
                .Select(GUIDToAssetPath)
                .Where(s => s.StartsWith(folderPath))
                .ToList();

            for (int i = 0; i < scenes.Count; i++)
            {
                EditorSceneManager.OpenScene(scenes[i], i == 0 ? OpenSceneMode.Single : OpenSceneMode.Additive);
            }
        }
    }
}