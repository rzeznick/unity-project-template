﻿using static System.IO.Directory;
using static System.IO.Path;
using static UnityEngine.Application;

namespace Metzger.Tools.Setup
{
    public static class Folders
    {

        public static void CreateDirectories(string root, params string[] folders)
        {
            var fullPath = Combine(dataPath, root);
            foreach (var folder in folders)
            {
                CreateDirectory(Combine(fullPath, folder));
            }
        }
    }
}