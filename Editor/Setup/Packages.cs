﻿using System.Net.Http;
using System.Threading.Tasks;
using UnityEditor.PackageManager;
using static System.IO.File;
using static System.IO.Path;
using static UnityEngine.Application;

namespace Metzger.Tools.Setup
{
    public static class Packages 
    { 
        public static async void ReplacePackages(string id)
        {
            var url = GetSnippetUrl(id);
            var content = await GetContent(url);
            ReplacePackageFile(content);
        }

        private static string GetSnippetUrl(string id, string user = "rzeznick") => $"https://bitbucket.org/!api/2.0/snippets/{user}/k7e8Gj/{id}/files/manifest.json";

        private static async Task<string> GetContent(string url)
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(url);
                var content = await response.Content.ReadAsStringAsync();
                return content;
            }
        }

        private static void ReplacePackageFile(string content)
        {
            var manifestPath = Combine(dataPath, "../Packages/manifest.json");
            WriteAllText(manifestPath, content);
            Client.Resolve();
        }
    }
}